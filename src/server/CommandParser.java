package server;

import java.io.IOError;

import command.*;
import exception.IllegalNameCommandException;


public class CommandParser {
	/*
	public final static String CLEAR = "clear";
	public final static String ALERT = "alert";
	public final static String PUSH = "push";
	*/
	
	
	
	public static Command parseCommand (String commandName) throws IllegalNameCommandException{
		
		CommandRegister cr =  CommandRegister.getInstance();
		
		return cr.createCommand(commandName);
		
	}
}
