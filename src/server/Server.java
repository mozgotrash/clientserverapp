/**
 * 
 */
/**
 * @author mozgotrash
 *
 */
package server;

import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

import org.omg.CORBA.portable.InputStream;

import command.*;
import command.annotations.CommandName;
import command.annotations.RegisterCommand; 


public class Server {
	public final static int PORT = 7070;
	
	/**
	 * @param str
	 * @throws SecurityException 
	 * @throws NoSuchFieldException 
	 * @throws IllegalAccessException 
	 * @throws IllegalArgumentException 
	 */
	public static void main(String str[]) 
			throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException {
		
		DeafultCommandProcessor.init();
		
		ServerSocket sSocket = null;
		try {
			sSocket = new ServerSocket(PORT);
			System.out.println("Waiting massage from Client...");
			Socket socket = sSocket.accept();
			
			InputStream sin = socket.getInputStream();	
			DataInputStream in = new DataInputStream(sin);

			String line;
			while(true) {
				line = in.readUTF();
				System.out.println("Client> " + line);
				
				Command command = CommandParser.parseCommand(line);
				command.execute();
			}
		}catch(Exception x) {
			x.printStackTrace();
		}
	
		//Выделить в отдельный класс DefaultCommandProcessor.init()
		//Добавить тесты для аннотаций и DeafultCommandProcessor.
		//register.createCommand("clear").execute();
		//register.createCommand("erase").execute();
	}
}



