package server;

import command.Command;
import command.CommandConfig;
import command.CommandRegister;
import command.annotations.CommandName;
import command.annotations.RegisterCommand;

public class DeafultCommandProcessor {
	
	static void init(){
		Class<? extends Command>[] commands = CommandConfig.class
				.getAnnotation(RegisterCommand.class)
				.value();
		
		
		CommandRegister register = CommandRegister.getInstance();
		
		for(Class<? extends Command> clazz: commands) {
			//TODO обрабатывать случаи, когда аннотация не указана - выводить информативное сообщение
			String[] aliases = clazz.getAnnotation(CommandName.class).value();
			try {
				register.register(clazz, aliases);
			}catch (NullPointerException e) {
				System.out.println("Класс команды: " + clazz.getName() + "не имеет анотаций");
			}
		}
	}
}
