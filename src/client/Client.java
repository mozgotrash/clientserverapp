/**
 * 
 */
/**
 * @author mozgotrash
 *
 */
package client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.Socket;

public class Client {
	public static final int PORT = 7070;
	public static final String HOST = "localhost";
	
	public static void main(String str[]) {
		Socket socket = null;
		try {
			socket = new Socket(HOST, PORT);
			try(InputStream sin = socket.getInputStream();
				OutputStream sout = socket.getOutputStream()){
				DataOutputStream out = new DataOutputStream(sout);
				System.out.println("Client>");
				BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
				while(true) {
					String line = br.readLine();
					out.writeUTF(line);
					out.flush(); // очищает буффер
				
				}
			}
		}catch(Exception x) {
			x.printStackTrace();

		}
	}
}