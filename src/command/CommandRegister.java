package command;

import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Resource;

import exception.DuplicateCommandNameException;
import exception.IllegalNameCommandException;

public class CommandRegister {
	
	private Map<String, Class<? extends Command>> commands;

	private static final CommandRegister commandRegister = new CommandRegister();
	
	public static CommandRegister getInstance() {
		//if(commandRegister == null) {
		//	commandRegister = new CommandRegister();
		//}
		
		return commandRegister;
	}
	
	private CommandRegister() {
		super();
		commands = new HashMap<>();
	}
	
	public void register(final Class<? extends Command> clazz, final String... commandAliases) {
		for(String name: commandAliases) {
			final String lowerCaseCommandName = name.toLowerCase();
			
			if(commands.containsKey(lowerCaseCommandName)) {
				throw new DuplicateCommandNameException(name);
			}
					
			commands.put(lowerCaseCommandName, clazz);
		}	
	}
	
	public Command createCommand(final String commandName) {
		final String lowerCaseCommandName = commandName.toLowerCase();
		
		Class<? extends Command> clazz = commands.get(commandName);
		
		if(clazz == null) {
			throw new IllegalNameCommandException(commandName);
		}
		
		Command command = null;
		
		try {
			Constructor<? extends Command> ctor = clazz.getConstructor();
			command = ctor.newInstance();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return command;
	}

}
