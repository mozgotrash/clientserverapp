package command;

import command.annotations.RegisterCommand;

@RegisterCommand({
	AlertCommand.class,
	ClearCommand.class,
	PushCommand.class
})
public class CommandConfig {

}
