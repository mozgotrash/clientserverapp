package command;

import command.annotations.CommandName;

@CommandName("alert")
public class AlertCommand implements Command{
	@Override
	public void execute() {
		System.out.println("Alert");
	}

}
