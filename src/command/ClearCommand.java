package command;

import command.annotations.CommandName;

@CommandName({"clear", "erase"})
public class ClearCommand implements Command {
	@Override
	public void execute() {
		System.out.println("Cleared");
	}

}
