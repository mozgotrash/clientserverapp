package exception;

public class IllegalNameCommandException extends RuntimeException{

	public IllegalNameCommandException(String message) {
		super(message);	
	}
}

